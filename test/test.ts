"use strict";

import { expect } from "chai";
import { HtmlEngine, DOMEngine, html, head, body, div } from "../index";

const app = html(head(), body({}, div({ class: "div-class" })));

describe("Html Engine", () => {
  it("should return html as string", () => {
    const engine = new HtmlEngine();
    const html = engine.render(app);
    expect(html).to.equal(
      `<html><head></head><body><div class="div-class"></div></body></html>`
    );
  });
});
