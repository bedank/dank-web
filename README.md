# DankScript
*By Lucas Goraieb 2018*
Use this to compile the DankScript library.

## To start:
To start, for now:
```
npm install
npm test
```

## Note:
If you want to develop on this machine, run in another tab:
```tsc -w```
