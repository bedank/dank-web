"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var index_1 = require("../index");
var app = index_1.html(index_1.head(), index_1.body({}, index_1.div({ class: "div-class" })));
describe("Html Engine", function () {
    it("should return html as string", function () {
        var engine = new index_1.HtmlEngine();
        var html = engine.render(app);
        chai_1.expect(html).to.equal("<html><head></head><body><div class=\"div-class\"></div></body></html>");
    });
});
