export declare type Trigger = string | string[];
export interface Attributes {
    trigger?: Trigger;
}
export interface Node {
    tag: string;
    content?: any[];
    attributes?: any;
    triggers?: {
        [key: string]: boolean;
    };
    element?: HTMLElement;
}
export declare type NodeFunction = () => number | string | Node | Node[];
export declare type Content = number | string | Node | Node[] | NodeFunction;
