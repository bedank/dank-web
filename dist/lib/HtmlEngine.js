"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HtmlEngine = /** @class */ (function () {
    function HtmlEngine() {
    }
    HtmlEngine.prototype.render = function (content) {
        var _this = this;
        if (content instanceof Array) {
            var out_1 = "";
            content.map(function (node) { return (out_1 += _this.render(node)); });
            return out_1;
        }
        else if (typeof content == "function") {
            return this.render(content());
        }
        else if (typeof content == "object") {
            var node = content;
            var tagDefinition = "<" + node.tag;
            if (node.attributes) {
                for (var _i = 0, _a = Object.keys(node.attributes); _i < _a.length; _i++) {
                    var key = _a[_i];
                    var attribute = node.attributes[key];
                    if (key.indexOf("on") == 0) {
                        //element[key] = attribute.toString(); //TODO support scripts
                    }
                    else if (key != "trigger" && key != "tag") {
                        var value = typeof attribute == "function" ? attribute() : attribute;
                        tagDefinition += " " + key + '="' + value + '"';
                    }
                }
            }
            tagDefinition += ">";
            var innerHTML = "";
            if (node.content)
                innerHTML = this.render(node.content);
            return tagDefinition + innerHTML + "</" + node.tag + ">";
        }
        else {
            return content.toString();
        }
    };
    return HtmlEngine;
}());
exports.default = HtmlEngine;
