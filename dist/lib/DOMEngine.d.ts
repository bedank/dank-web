import { Content } from "./DankWeb";
export default class DOMEngine {
    private rootNode;
    private rootElement;
    private triggered;
    private triggerTimeout;
    trigger(...triggers: string[]): void;
    constructor(element: HTMLElement, root: Content);
    render(): void;
    private recurse(parent, content, triggered?);
}
