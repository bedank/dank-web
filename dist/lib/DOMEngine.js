"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DOMEngine = /** @class */ (function () {
    function DOMEngine(element, root) {
        this.triggered = {};
        this.rootNode = root;
        this.rootElement = element;
    }
    DOMEngine.prototype.trigger = function () {
        var _this = this;
        var triggers = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            triggers[_i] = arguments[_i];
        }
        this.triggered = this.triggered || {};
        for (var _a = 0, triggers_1 = triggers; _a < triggers_1.length; _a++) {
            var trigger = triggers_1[_a];
            this.triggered[trigger] = true;
        }
        if (this.triggerTimeout) {
            clearTimeout(this.triggerTimeout);
        }
        this.triggerTimeout = setTimeout(function () {
            _this.recurse(_this.rootElement, _this.rootNode, Object.keys(_this.triggered));
            delete _this.triggered;
        }, 0);
    };
    DOMEngine.prototype.render = function () {
        this.recurse(this.rootElement, this.rootNode);
    };
    DOMEngine.prototype.recurse = function (parent, content, triggered) {
        var _this = this;
        if (content instanceof Array) {
            content.map(function (node) { return _this.recurse(parent, node, triggered); });
        }
        else if (typeof content == "function") {
            this.recurse(parent, content(), triggered);
        }
        else if (typeof content == "object") {
            var node = content;
            var element = node.element || document.createElement(node.tag);
            if (!element.parentNode) {
                node.element = element;
                parent.appendChild(element);
            }
            if (node.triggers) {
                var hasTrigger = false;
                if (triggered) {
                    for (var _i = 0, triggered_1 = triggered; _i < triggered_1.length; _i++) {
                        var trigger = triggered_1[_i];
                        if (node.triggers[trigger]) {
                            hasTrigger = true;
                            break;
                        }
                    }
                }
                if (hasTrigger) {
                    //while (element.childElementCount > 0) element.removeChild(element.children[0]);
                    element.innerHTML = "";
                }
            }
            for (var _a = 0, _b = Object.keys(node.attributes); _a < _b.length; _a++) {
                var key = _b[_a];
                var attribute = node.attributes[key];
                if (key.indexOf("on") == 0) {
                    element[key] = attribute;
                }
                else if (key != "trigger") {
                    var value = typeof attribute == "function" ? attribute() : attribute;
                    element.setAttribute(key, value);
                }
            }
            if (node.content)
                this.recurse(element, node.content, triggered);
        }
        else {
            parent.innerHTML = content.toString();
        }
    };
    return DOMEngine;
}());
exports.default = DOMEngine;
