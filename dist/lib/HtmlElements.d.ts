import { Content, Node, Attributes } from "./DankWeb";
export declare type Attribute = string | (() => string);
export interface BodyAttributes extends Attributes {
    style?: string;
    class?: Attribute;
    onclick?(e: any): boolean;
}
export declare function html(...content: Content[]): Node;
export declare function head(...content: Content[]): Node;
export declare function body(attributes?: BodyAttributes, ...content: Content[]): Node;
export declare function div(attributes?: BodyAttributes, ...content: Content[]): Node;
export declare function span(attributes?: BodyAttributes, ...content: Content[]): Node;
export interface InputAttributes extends BodyAttributes {
    type: "text" | "submit" | "button";
    value: string;
    onsubmit?: (event: any) => void;
}
export declare function input(attributes?: InputAttributes): Node;
export interface TextAreaAttributes extends BodyAttributes {
    cols?: number;
    rows?: number;
}
export declare function textarea(attributes?: TextAreaAttributes, ...content: Content[]): Node;
export declare function h1(attributes?: BodyAttributes, ...content: Content[]): Node;
export declare function ul(attributes?: BodyAttributes, ...content: Content[]): Node;
export declare function li(attributes?: BodyAttributes, ...content: Content[]): Node;
export interface LinkAttributes extends BodyAttributes {
    href?: string;
}
export declare function a(attributes?: LinkAttributes, ...content: Content[]): Node;
export interface ImgAttributes extends BodyAttributes {
    src?: Attribute;
}
export declare function img(attributes?: ImgAttributes, ...content: Content[]): Node;
