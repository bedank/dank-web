"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function element(tag, attributes) {
    var content = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        content[_i - 2] = arguments[_i];
    }
    var node = {
        tag: tag,
        attributes: attributes,
        content: content
    };
    if (attributes && attributes.trigger) {
        node.triggers = {};
        if (typeof attributes.trigger == "string") {
            node.triggers[attributes.trigger] = true;
        }
        else {
            for (var _a = 0, _b = attributes.trigger; _a < _b.length; _a++) {
                var k = _b[_a];
                node.triggers[k] = true;
            }
        }
    }
    return node;
}
function html() {
    var content = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        content[_i] = arguments[_i];
    }
    return element.apply(void 0, ["html", undefined].concat(content));
}
exports.html = html;
function head() {
    var content = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        content[_i] = arguments[_i];
    }
    return element.apply(void 0, ["head", undefined].concat(content));
}
exports.head = head;
function body(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["body", attributes].concat(content));
}
exports.body = body;
function div(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["div", attributes].concat(content));
}
exports.div = div;
function span(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["span", attributes].concat(content));
}
exports.span = span;
function input(attributes) {
    return element("input", attributes);
}
exports.input = input;
function textarea(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["textarea", attributes].concat(content));
}
exports.textarea = textarea;
function h1(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["h1", attributes].concat(content));
}
exports.h1 = h1;
function ul(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["ul", attributes].concat(content));
}
exports.ul = ul;
function li(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["li", attributes].concat(content));
}
exports.li = li;
function a(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["a", attributes].concat(content));
}
exports.a = a;
function img(attributes) {
    var content = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        content[_i - 1] = arguments[_i];
    }
    return element.apply(void 0, ["img", attributes].concat(content));
}
exports.img = img;
