import { Content } from "./DankWeb";
export default class HtmlEngine {
    render(content: Content): string;
}
