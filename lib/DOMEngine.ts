import { Node, Content, NodeFunction, Attributes } from "./DankWeb";

export default class DOMEngine {

    private rootNode: Content;
    private rootElement: HTMLElement;
    private triggered: { [trigger: string]: boolean } = {};
    private triggerTimeout: any;

    trigger(...triggers: string[]) {
        this.triggered = this.triggered || {};
        for (let trigger of triggers) {
            this.triggered[trigger] = true;
        }
        if (this.triggerTimeout) {
            clearTimeout(this.triggerTimeout);
        }
        this.triggerTimeout = setTimeout(() => {
            this.recurse(this.rootElement, this.rootNode, Object.keys(this.triggered));
            delete this.triggered;
        }, 0);
    }

    constructor(element: HTMLElement, root: Content) {
        this.rootNode = root;
        this.rootElement = element;
    }

    render() {
        this.recurse(this.rootElement, this.rootNode);
    }

    private recurse(parent: HTMLElement, content: Content, triggered?: string[]) {
        if (content instanceof Array) {
            content.map(node => this.recurse(parent, node, triggered));
        } else if (typeof content == "function") {
            this.recurse(parent, (content as NodeFunction)(), triggered);
        } else if (typeof content == "object") {
            let node = content as Node;
            let element = node.element || document.createElement(node.tag);
            if (!element.parentNode) {
                node.element = element;
                parent.appendChild(element);
            }

            if (node.triggers) {
                let hasTrigger = false;
                if (triggered) {
                    for (let trigger of triggered) {
                        if (node.triggers[trigger]) {
                            hasTrigger = true;
                            break;
                        }
                    }
                }
                if (hasTrigger) {
                    //while (element.childElementCount > 0) element.removeChild(element.children[0]);
                    element.innerHTML = "";
                }
            }

            for (let key of Object.keys(node.attributes)) {
                let attribute = node.attributes[key];
                if (key.indexOf("on") == 0) {
                    (element as any)[key] = attribute;
                } else if (key != "trigger") {
                    let value = typeof attribute == "function" ? attribute() : attribute;
                    element.setAttribute(key, value);
                }
            }
            if (node.content) this.recurse(element, node.content, triggered);
        } else {
            parent.innerHTML = content.toString();
        }
    }
}