import { Content, Node, Attributes } from "./DankWeb";

export type Attribute = string | (() => string);

function element(tag: string, attributes?: Attributes, ...content: Content[]): Node {
    let node: Node = {
        tag: tag,
        attributes: attributes,
        content: content
    };
    if (attributes && attributes.trigger) {
        node.triggers = {};
        if (typeof attributes.trigger == "string") {
            node.triggers[attributes.trigger] = true;
        } else {
            for (let k of attributes.trigger) {
                node.triggers[k] = true;
            }
        }
    }
    return node;
}

export interface BodyAttributes extends Attributes {
    style?: string;
    class?: Attribute;
    onclick?(e: any): boolean;
}

export function html(...content: Content[]): Node {
    return element("html", undefined, ...content);
}

export function head(...content: Content[]): Node {
    return element("head", undefined, ...content);
}

export function body(attributes?: BodyAttributes, ...content: Content[]): Node {
    return element("body", attributes, ...content);
}

export function div(attributes?: BodyAttributes, ...content: Content[]): Node {
    return element("div", attributes, ...content);
}
export function span(attributes?: BodyAttributes, ...content: Content[]): Node {
    return element("span", attributes, ...content);
}
export interface InputAttributes extends BodyAttributes {
    type: "text" | "submit" | "button";
    value: string;
    onsubmit?: (event: any) => void;
}
export function input(attributes?: InputAttributes): Node {
    return element("input", attributes);
}
export interface TextAreaAttributes extends BodyAttributes {
    cols?: number;
    rows?: number;
}
export function textarea(attributes?: TextAreaAttributes, ...content: Content[]): Node {
    return element("textarea", attributes, ...content);
}
export function h1(attributes?: BodyAttributes, ...content: Content[]): Node {
    return element("h1", attributes, ...content);
}
export function ul(attributes?: BodyAttributes, ...content: Content[]): Node {
    return element("ul", attributes, ...content);
}
export function li(attributes?: BodyAttributes, ...content: Content[]): Node {
    return element("li", attributes, ...content);
}

export interface LinkAttributes extends BodyAttributes {
    href?: string;
}
export function a(attributes?: LinkAttributes, ...content: Content[]): Node {
    return element("a", attributes, ...content);
}

export interface ImgAttributes extends BodyAttributes {
    src?: Attribute;
}
export function img(attributes?: ImgAttributes, ...content: Content[]): Node {
    return element("img", attributes, ...content);
}
