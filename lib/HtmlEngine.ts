import { Node, Content, NodeFunction, Attributes } from "./DankWeb";

export default class HtmlEngine {
  render(content: Content): string {
    if (content instanceof Array) {
      let out = "";
      content.map(node => (out += this.render(node)));
      return out;
    } else if (typeof content == "function") {
      return this.render((content as NodeFunction)());
    } else if (typeof content == "object") {
      let node = content as Node;
      var tagDefinition = "<" + node.tag;
      if (node.attributes) {
        for (let key of Object.keys(node.attributes)) {
          let attribute = node.attributes[key];
          if (key.indexOf("on") == 0) {
            //element[key] = attribute.toString(); //TODO support scripts
          } else if (key != "trigger" && key != "tag") {
            let value =
              typeof attribute == "function" ? attribute() : attribute;
            tagDefinition += " " + key + '="' + value + '"';
          }
        }
      }
      tagDefinition += ">";
      let innerHTML = "";
      if (node.content) innerHTML = this.render(node.content);
      return tagDefinition + innerHTML + "</" + node.tag + ">";
    } else {
      return content.toString();
    }
  }
}
